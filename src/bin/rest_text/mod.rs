use actix_web::{web};

use crate::psql::*;
use crate::model::Person;

pub fn get(path: web::Path<i32>) -> String {
    let id = path.clone();
    println!("search for id: {}",id);
    let conn = gen_conn();
    let person = get_person(&conn, id);
    if let Some(p) = person {
        format!("ID: {}, Name: {}",p.id, p.name)
    } else {
        format!("Non person found for ID: {}",id)
    }
}

pub fn get_all() -> String {
    let conn = gen_conn();
    let result: Vec<Person> = get_all_person(&conn);
    println!("All: {:#?}",result);
    format!("All: {:#?}",result)
}

pub fn put(path: web::Path<String>) -> String {
    let name = path.clone();
    let conn = gen_conn();
    add_person(&conn, name.clone());
    println!("Added: {}",name);
    format!("Added: {}",name)
}