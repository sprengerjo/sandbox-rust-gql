use rustbreak::Database;

pub mod model;

fn main() {
    let db: Database<String> = Database::open("my_contacts").unwrap();

//    db.insert("Lapfox", "lapfoxtrax.com").unwrap();
//    db.insert("Rust", "github.com/rust-lang/rust").unwrap();
    db.insert("toto", "my-host.com/toto").unwrap();

    // we need to be explicit about the kind of type we want as println! is
    // generic
    let rust : String = db.retrieve("toto").unwrap();
    println!("You can find toto at: {}", rust);
    db.flush().unwrap();
}