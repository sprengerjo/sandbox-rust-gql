#![allow(dead_code)]

mod psql;
pub mod model;

fn main() {
    let conn = psql::gen_conn();

//    init_table(&conn);

//    add_person(&conn, String::from("Lulu"));
//    add_person(&conn, String::from("Riri"));
//    add_person(&conn, String::from("Tata"));

    let p = psql::get_all_person(&conn);
//    let p = psql::get_person(&conn, 8);

    let json = serde_json::to_string(&p).unwrap();
    println!("{}",json)
}