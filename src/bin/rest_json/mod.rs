use actix_web::{web, Responder, Error, HttpRequest, HttpResponse};

use crate::psql::*;
use crate::model::Person;

impl Responder for Person {
    type Error = Error;
    type Future = Result<HttpResponse, Error>;

    fn respond_to(self, _req: &HttpRequest) -> Self::Future {
        let body = serde_json::to_string(&self)?;

        // Create response and set content type
        Ok(HttpResponse::Ok()
            .content_type("application/json")
            .body(body))
    }
}

// Hack to serialize many Persons !?
struct Persons {
    persons: Vec<Person>
}

impl Responder for Persons {
    type Error = Error;
    type Future = Result<HttpResponse, Error>;

    fn respond_to(self, _req: &HttpRequest) -> Self::Future {
        let body = serde_json::to_string(&self.persons)?;

        // Create response and set content type
        Ok(HttpResponse::Ok()
            .content_type("application/json")
            .body(body))
    }
}

#[derive(Deserialize)]
pub struct QueryId {
    id:i32
}

pub fn get(body: web::Json<i32>) -> impl Responder {
    let query: i32 = body.0;
    println!("search for id: {}",query);
    let conn = gen_conn();
    let person = get_person(&conn, query).unwrap();
    person
}

pub fn get_all() -> impl Responder {
    println!("get all...");
    let conn = gen_conn();
    let persons: Vec<Person> = get_all_person(&conn);
    Persons {persons }
}

use serde::{Serialize, Deserialize};

#[derive(Deserialize, Serialize)]
pub struct QueryName {
    name:String
}

impl Responder for QueryName {
    type Error = Error;
    type Future = Result<HttpResponse, Error>;

    fn respond_to(self, _req: &HttpRequest) -> Self::Future {
        let body = serde_json::to_string(&self)?;

        // Create response and set content type
        Ok(HttpResponse::Ok()
            .content_type("application/json")
            .body(body))
    }
}

pub fn put(body: web::Json<QueryName>) -> impl Responder {
    let query: QueryName = body.0;
    println!("Put name: {}",&query.name);
    let conn = gen_conn();
    add_person(&conn, query.name.clone());
    query
}