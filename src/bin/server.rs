#![allow(dead_code, unused_imports, unused_variables)]

use actix_web::{App, web, HttpServer, HttpResponse, Error};

mod psql;
mod gql;
mod rest_text;
mod rest_json;
pub mod model;

use std::sync::Arc;
use futures::future::Future;
use juniper::http::{GraphQLRequest,GraphQLResponse};
use juniper::DefaultScalarValue;

fn main() {

//    fn graphql_async(
//        st: web::Data<Arc<gql::Schema>>,
//    ) -> impl Future<Item = HttpResponse, Error = Error> {
//        web::block(move || {
//            let res = data.execute(&st, &());
//            Ok::<_, serde_json::error::Error>(serde_json::to_string(&res)?)
//        })
//            .map_err(Error::from)
//            .and_then(|user| {
//                Ok(HttpResponse::Ok()
//                    .content_type("application/json")
//                    .body(user))
//            })
//    }
    struct MyData {
        ctx: gql::Ctx,
        schema: gql::Schema
    }

    fn graphql(
        tx: web::Data<MyData>,
        data: web::Json<GraphQLRequest>,
    ) -> HttpResponse {
        let gql_res: GraphQLResponse<DefaultScalarValue> = data.execute(&tx.schema, &tx.ctx);
        HttpResponse::Ok().json(gql_res)
    }

    let server = HttpServer::new(|| {
        App::new()
            .data(MyData {ctx: gql::Ctx {conn: psql::gen_conn()}, schema: gql::create_schema()})
            .service(
                web::scope("/text")
                    .route("/get/{id}", web::get().to(rest_text::get))
                    .route("/all", web::get().to(rest_text::get_all))
                    .route("/put/{name}", web::put().to(rest_text::put))
            )
            .service(
                web::scope("/json")
                    .route("/get", web::get().to(rest_json::get))
                    .route("/all", web::get().to(rest_json::get_all))
                    .route("/put", web::put().to(rest_json::put))
            )
            .service(
                web::resource("/graphql").route(web::post().to(graphql))
            )
    });

    println!(" ===> Starting server ...");
    server
        .bind("localhost:8080")
        .unwrap()
        .run()
        .unwrap();

    println!(" ===> End!")
}

