use serde::{Serialize, Deserialize};

#[derive(Debug, Serialize, Deserialize, juniper::GraphQLObject)]
#[graphql(description="A lambda person")]
pub struct Person {
    pub id: i32,
    pub name: String,
}