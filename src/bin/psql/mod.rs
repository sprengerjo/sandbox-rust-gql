use postgres::{Connection, TlsMode};
use super::model::Person;

pub fn gen_conn() -> Connection {
                        //protocol://username:password@host:port
    Connection::connect("postgres://postgres:admin@localhost:5432", TlsMode::None).unwrap()
}

pub fn init_table(conn: &Connection) -> () {
    conn.execute("CREATE TABLE person (
                    id              SERIAL PRIMARY KEY,
                    name            VARCHAR NOT NULL
                  )", &[]).unwrap();
}

pub fn add_person(conn: &Connection, name: String) -> u64 {
    conn.execute("INSERT INTO person (name) VALUES ($1)", &[&name]).unwrap()
}

pub fn get_person(conn: &Connection, id:i32) -> Option<Person> {
    let result = conn.query("SELECT id, name FROM person WHERE id = $1", &[&id]);
    if let Ok(rows) = result {
        let row = rows.get(0);
        Some(Person {
            id: row.get(0),
            name: row.get(1)
        })
    } else {
        None
    }
}

pub fn get_all_person(conn: &Connection) -> Vec<Person> {
    let result = conn.query("SELECT id, name FROM person", &[]).unwrap();
    let iter = result
        .iter()
        .map(|r|
            Person {
                id: r.get(0),
                name: r.get(1)
            }
        );
    use std::iter::FromIterator;
    Vec::from_iter(iter)
}