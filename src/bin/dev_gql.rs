#![allow(dead_code, unused_imports, unused_variables)]
use juniper::{EmptyMutation, Variables, IntrospectionFormat};

pub mod gql;
pub mod psql;
pub mod model;

fn main() {

//    println!("Hello!");
//

//    let toto = model::Person {
//        id:69,
//        name: String::from("Jojo")
//    };

    let conn = psql::gen_conn();
    let ctx = gql::Ctx {conn};
    let s = gql::create_schema();

    let res = juniper::execute(
        "query { getPerson(id: 13) { id name } }",
        None,
        &s,
        &Variables::new(),
        &ctx
    );

    println!("RESULT: {:#?}",res.unwrap().0);

//    let json_schema = juniper::introspect(
//        &s,
//        &ctx,
//        IntrospectionFormat::default(),
//    ).unwrap().0;
//
//    let json = serde_json::to_string_pretty(&json_schema).unwrap();

//    println!("SCHEMA: {}", json);
}