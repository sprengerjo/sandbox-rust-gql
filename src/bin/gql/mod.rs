use juniper::{FieldResult, EmptyMutation};
use postgres::{Connection};
use serde::{Serialize, Deserialize};


use super::psql::*;
use crate::model::Person;

pub struct Ctx {
    pub conn:Connection
}

impl juniper::Context for Ctx {}


impl Ctx {
    fn get_person(&self, id:i32) -> Option<Person> {
        let p = get_person(&self.conn, id);
        p
    }
    fn get_all_person(&self) -> Vec<Person> {
        get_all_person(&self.conn)
    }
}

pub struct Query;

#[juniper::object(
    Context = Ctx,
)]
impl Query {

    /// For the moment retrieve a fake id
    fn get_person(ctx: &Ctx, id:i32) -> FieldResult<Person> {
        let p = ctx.get_person(id);
        Ok(
            p.unwrap()
        )
    }

    fn get_all_person(ctx: &Ctx) -> FieldResult<Vec<Person>> {
        let res = ctx.get_all_person();
        Ok(
            res
        )
    }

    fn get_test(ctx: &Ctx) -> FieldResult<Test> {
        Ok(Test{ value: 42})
    }
}

pub type Schema = juniper::RootNode<'static, Query, EmptyMutation<Ctx>>;
//    let s = juniper::RootNode::new(gql::Query, EmptyMutation::new());

pub fn create_schema() -> Schema {
    Schema::new(Query, EmptyMutation::new())
}

//type Schema = juniper::RootNode<'static, Query, EmptyMutation<Ctx>>;

#[derive(Debug, Serialize, Deserialize, juniper::GraphQLObject)]
#[graphql(description="A stupid test!!!")]
pub struct Test {
    value: i32
}